// Mini-activity session 29

// drop the collection if you want open the database and then open shell in database
db.dropDatabase()
// >> this will return a boolean


db.products.insertMany([
		{
			name: "Iphone X",
	        price: 30000,
	        isActive: true
	    },
	    {   	
	        name: "Samsung Galaxy S21",
	        price: 51000,
	        isActive: true
	    },
	    {
	        name: "Razer Blackshark V2X",
	        price: 2800,
	        isActive: false
	    },
	    {
	        name: "RAKK Gaming Mouse",
	        price: 1800,
	        isActive: true
	    },  
	    {
	        name: "Razer Mechanical Keyboard",
	        price: 4000,
	        isActive: true
	    },
	])


// any one of this two
db.products.find()
db.getCollection('products').find({})





// Query Operators and Field Projection

// Query Operators
	// for flexible querying in MongoDB
	// instead of finding exact values, we can use query operators that define conditions instead of just specific criterias

// that tha query operators
	// >> $gt, $lt, $gte and $lte

	// $gt - greater than
	db.products.find({price: {$gt: 3000}});

	// $lt - less than
	db.products.find({price: {$lt: 4000}});

	// $gte - greater than or equal
	db.products.find({price: {$gte: 30000}});

	// $lte - less than or equal
	db.products.find({price: {$lte: 2800}});

	// expanding the search criteria for updating or deleting

	db.products.updateMany(
			{
				price: {$gte: 30000}
			},
			{
				$set: {isActive: false}
			}
		)


// Mini - activity

db.users.insertMany([
		{
			firstName: "Mary Jane",
	        lastName: "Watson",
	        email: "mjtiger@gmail.com",
	        password: "tigerjackpot15",
	        isAdmin: false
	    },
	    {

	        firstName: "Gwen",
	        lastName: "Stacy",
	        email: "stacyTech@gmail.com",
	        password: "stacyTech1991",
	        isAdmin: true
	    },
	    {

	        firstName: "Peter",
	        lastName: "Parker",
	        email: "peterWebDev@gmail.com",
	        password: "webDeveloperPeter",
	        isAdmin: true
	    },
	    {

	        firstName: "Jonah",
	        lastName: "Jameson",
	        email: "jjjameson@gmail.com",
	        password: "spideyisamenace",
	        isAdmin: false
	    },
	    {

	        firstName: "Otto",
	        lastName: "Octavius",
	        email: "ottoOctopi@gmail.com",
	        password: "docOck15",
	        isAdmin: true
	    }
	])

// any one of this two
db.users.find()
db.getCollection('users').find({})

// $regex - will allows us to find documents in which it will match the characters / pattern of the characters we are looking for

// find who users that have only capital O
db.users.find({firstName: {$regex: 'O'}})

// $options
db.users.find({lastName: {$regex: 'o', $options: '$i'}})

// dind documents that matches a specific word
db.users.find({email: {$regex: 'web', $options: '$i'}})



// Mini - Activity
db.products.find({name: {$regex: 'razer', $options: '$i'}})
db.products.find({name: {$regex: 'rakk', $options: '$i'}})

// $or and $and
	// $or operator - logical operation wherein we can look or find a document which can satisfy at least 1 condition

	// ang mag tu true is 1 only
	// condition||condition === true


	db.products.find({
		$or :[
			{
				name: {$regex: 'x', $options: '$i'}
			},
			{
				price: {$lte: 10000}
			},
			]
	})
	// result: all products shown because item with x and less than 10000

	db.products.find({
		$or: [
			{
				name: {$regex: 'x', $options: '$i'}
			},
			{
				price: 30000
			}
		]
	})
	// result: 3 products because the price 3000 and the x letter lower and upper

	db.users.find({
		$or: [
				{
				// not case sensitive
				firstName: {$regex: 'a', $options: '$i'}
			},
			{
				isAdmin: true
			}
		]
	})

// Mini - activity
	db.users.find({
		$or: [
			{
				lastName: {$regex: 'w', $options: '$i'}
			},
			{
				isAdmin: false
			}
		]
	})

// this is to see what is the highest product and the lowest product
db.products.find({
		$or :[
			{
				price: {$gte: 51000}
			},
			{
				price: {$gte: 10000}
			},
			]
	})

// $and - logical operation wherein we can look or find documents which can satisfy both conditions

db.products.find({
	$and: [
		{
			name: {$regex: 'razer', $options: '$i'}
		},
		{
			price: {$gte: 3000}
		}
	]
})

db.users.find({
	$and: [
		{
			lastName: {$regex: 'w', $options: '$i'}
		},
		{
			isAdmin: false
		}
	]
})

// Field Projection
	// allows us to hide / show properties / field of the returned documents after query

	// fields projection
		// 0 means hide
		// 1 means show

	// _id and password will be empty
	db.users.find({}, {_id: 0, password: 0})

	// appear only email and not admi
	db.users.find({isAdmin: false}, {_id: 0, email: 1})
	// appear id, firstname and lastname and not admin
	db.users.find({isAdmin: false}, {firsName: 1, lastName: 1})

	db.products.find(
		{
			price: {$gte: 10000}
		},
		{
			_id: 0, name: 1, price: 1
		}
)